# Pacemaker

#    ___                               _             
#   / _ \__ _  ___ ___ _ __ ___   __ _| | _____ _ __ 
#  / /_)/ _` |/ __/ _ \ '_ ` _ \ / _` | |/ / _ \ '__|
# / ___/ (_| | (_|  __/ | | | | | (_| |   <  __/ |   
# \/    \__,_|\___\___|_| |_| |_|\__,_|_|\_\___|_|   
                                                   
######################
## v0 - Development ##
######################


Setup a fully functioning pacemaker instance in minutes! 

Add Oracle instances, HTTPd instances, Nginx instances! Quick and efficient!
