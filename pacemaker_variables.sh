#!/bin/bash

# This is the primary configuration file for Pacemaker.

# Set variables in this configuration file in order to use them in the main script. [ Please edit! ]

HOST1="<yourHostname1>"
HOST2="<yourHostname2>"
#HOST3="<optionalHostname3>" # The script will ask a question if there are more than two hosts, if this is uncommented and true, say yes in the script. 


# Pacemaker Values


# Quorum Values
QUORUM_TOKEN="18000"
QUORUM_WAIT_FOR_ALL="1"

# Additional Pacemaker Resources 
MYSQL_SETUP=""



#### Do not edit anything below or the script may potentially break! ####
# Default variables for script environment 
OS=""
TERMINAL=$(tty)
HEIGHT=15
WIDTH=40
CHOICE_HEIGHT=4
BACKTITLE="HuntMilamber: Pacemaker Install"
TITLE="Pacemaker Installation"
MENU="Choose one of the following options:"

OPTIONS=(1 "Install"
         2 "Uninstall"
         3 "About"
         4 "Exit")
CHOICE=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >"$TERMINAL")
# Set BASH ANSI colors
GREEN='\033[1;32m' # Light Green
RED='\033[0;31m' # Red
BLUE='\033[1;34m' # Blue
YELLOW='\033[1;33m' # Yellow
NC='\033[0m' # No Color
RN="${RED}[-]${NC}" # Red Negative Symbol (Error)
BE="${BLUE}[!]${NC}" # Blue Exclamation Mark (Info)
GP="${GREEN}[+]${NC}" # Green Positive Symbol (Success)
YA="${YELLOW}[*]${NC}" # Yellow Asterisk Symbol (Warning)