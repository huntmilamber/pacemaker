#!/bin/bash

# This is the primary installation script for Pacemaker.

# Ensure you have set the correct values in pacemaker_variables.sh as this script will source all variables from that file.

# Inherit values from configuration var file
source pacemaker_variables.sh

# Setup Dialog Menu for running script:

case $CHOICE in
        1)
            echo -e "${BE} You selected to Install Pacemaker."
            ;;
        2)
            echo -e "${BE} You selected to Uninstall Pacemaker"
            ;;
        3)
            echo "About"
            ;;
        4)
            echo -e "Goodbye!"
esac

# Logic for OS Detection:
function os_detect() {
    
    if [ -f /etc/redhat-release ];
        then
        OS="RHEL"
        echo -e "${GP} Successfully set OS as RHEL-based. (.rpm)"
    elif [ -f /etc/lsb-release ];
        then
        OS="Debian/Ubuntu"
        echo -e "${GP} Successfully set OS as Debian-based. (.deb)"
    else 
        echo -e "${RN} Could not determine OS. Sorry! :("
    fi
}

# Systems Setup
function os_prereq_setup {

    if [ $OS == "RHEL" ]; then
        echo -e "${GP} Proceeding with RHEL-based system setup."

        # Setup pre-req's and update
        yum -y update; yum install -y pacemaker pcs resource-agents fence-agents-all device-mapper-multipath

        # Enable and startup new services
        systemctl 

    elif [ $OS == "Debian/Ubuntu" ]; then
        echo -e "${GP} Proceeding with Debian-based system setup."
    fi    
}
os_detect
